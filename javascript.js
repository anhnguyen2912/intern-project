  let texts = [
    "designers.",
    "developers.",
    "founders."
  ];
  let currentTextIndex = 0;
  let currentText = texts[currentTextIndex];
  let title = document.getElementById("import-text");
  let i = 0;
  let isTyping = true;

  function typeWriter() {
    if (i < currentText.length && isTyping) {
      title.innerHTML += currentText.charAt(i);
      i++;
      setTimeout(typeWriter, 50);
    } else {
      setTimeout(function() {
        clearText();
      }, 2000);
    }
  }

  function clearText() {
    if (i >= 0) {
      title.innerHTML = currentText.substring(0, i);
      i--;
      setTimeout(clearText, 50);
    } else {
      currentTextIndex++;
      if (currentTextIndex >= texts.length) {
        currentTextIndex = 0;
      }
      currentText = texts[currentTextIndex];
      isTyping = true;
      setTimeout(typeWriter, 1000);
    }
  }

// window.onload = typeWriter;
 document.addEventListener("DOMContentLoaded", function() {
  // Your typeWriter function and other related code here
  typeWriter(); // Call the typeWriter function once the DOM is fully loaded
});




//  // js for package
const isAnual = document.getElementById("toggle");
const swap = document.getElementById("package");
// Function to delay for a specified amount of time
function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
// Add event listener to watch for changes in the toggle
isAnual.addEventListener('change', async function() {
  let currentPrice = parseFloat(swap.textContent);
  let targetPrice = isAnual.checked ? 49 : 29;
  if (currentPrice === targetPrice) { 
    return; // If the target price is the same as the current price, no need to transition
  } else {
    let step = currentPrice < targetPrice ? 1 : -1;
    while (currentPrice !== targetPrice) {
      currentPrice += step;
      swap.textContent = currentPrice;
      await delay(50);
    }
  }

});
// about
// Get the form and input elements
const form = document.getElementById('userform');
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('pass');
const nameError = document.getElementById('text-name');
const emailError = document.getElementById('text-email');
const passwordError = document.getElementById('text-pass');
const submitButton = document.getElementById('submit');
// Function to validate email format
function isValidEmail(email) {
  return /\S+@\S+\.\S+/.test(email);
}
// Add event listener for form submission
form.addEventListener('submit', function(event) {
  event.preventDefault(); // Prevent default form submission
  let errors = false;
  // Validate name
  if (nameInput.value === '') {
    nameError.textContent = 'Name is required';
    errors = true;
  } else {
    nameError.textContent = '';
  }
  // Validate email
  if (emailInput.value === '' || !isValidEmail(emailInput.value)) {
    emailError.textContent = 'Please enter a valid email address';
    errors = true;
  } else {
    emailError.textContent = '';
  }
  // Validate password
  if (passwordInput.value === '' || passwordInput.value.length < 8) {
    passwordError.textContent = 'Password must be at least 8 characters';
    errors = true;
  } else {
    passwordError.textContent = '';
  }
  // If there are no validation errors, log the values to the console
  if (!errors) {
    console.log('Name: ' + nameInput.value);
    console.log('Email: ' + emailInput.value);
    console.log('Password: ' + passwordInput.value);
  }
});
document.addEventListener('DOMContentLoaded', function () {
  const inputElements = document.querySelectorAll('.input-element');
  const formTitles = document.querySelectorAll('.form-title');
  const inputElement = inputElements.forEach(function (inputElement) {
    inputElement.addEventListener('focus', function () {
      inputElement.classList.add('selected');
      if (inputElement.id === 'name') {
        formTitles[0].classList.add('active');
      }
      if (inputElement.id === 'email') {
        formTitles[1].classList.add('active');
      }
      if (inputElement.id === 'pass') {
        formTitles[2].classList.add('active');
      }
      
    });
    inputElement.addEventListener('blur', function () {
      inputElement.classList.remove('selected');
      if (inputElement.id === 'name') {
        formTitles[0].classList.remove('active');
      }
      if (inputElement.id === 'email') {
        formTitles[1].classList.remove('active');
      }
      if (inputElement.id === 'pass') {
        formTitles[2].classList.remove('active');
      }
      
    });
  });
  



});
//scroll
window.addEventListener('scroll', function() {
  var element =  document.querySelector('.about');
  const rect = element.getBoundingClientRect();
  var position = rect.top;

  // Adjust the value below to determine when the fade-in effect should start
  var offset = window.innerHeight - 100;

  if (position < offset) {
    element.classList.add('fade-in');
  }
});










const review = document.querySelector(".review__container");
const slideList = review.querySelector(".slides");
const items = Array.from(slideList.children);

const prevButton = review.querySelector("#prev");
const nextButton = review.querySelector("#next");

const first = slideList.firstElementChild;
const last = slideList.lastElementChild;

const imagereview = document.querySelector(".review__list-img");
const image = [
    "https://landkit.goodthemes.co/assets/img/photos/photo-1.jpg",
    "https://landkit.goodthemes.co/assets/img/photos/photo-26.jpg",
];

let index = 1;
let pages = 0;
let dragStart = null;

copyItem = (element, refElement) => {
    const clone = element.cloneNode(true);
    slideList.insertBefore(clone, refElement);
    return clone;
};

TurnOffRootDragging = (element) => {
    element.draggable = false;
};

transitionPaused = (callback) => {
    let scheduled = null;

    return () => {
        if (scheduled) {
            window.cancelAnimationFrame(scheduled);
        }

        scheduled = window.requestAnimationFrame(() => {
            slideList.style.transition = "none";
            callback();

            scheduled = window.requestAnimationFrame(() => {
                slideList.style.transition = "";
                scheduled = null;
            });
        });
    };
};

toggleOff = (disabled) => {
    prevButton.disabled = nextButton.disabled = disabled;
};

getpages = () => {
    return items.slice(0, index).reduce((width, element) => width + element.clientWidth, 0);
};

translateSlides = (deltaX = 0) => {
    slideList.style.transform = `translateX(${-pages + deltaX}px)`;
};

SlidesTransition = (newIndex = index) => {
    index = newIndex;
    pages = getpages();
    translateSlides();
    if (index === 2) {
        imagereview["src"] = image[1];
    } else {
        imagereview["src"] = image[0];
    }
};

fakeInfinity = () => {
    switch (index) {
        case 0:
            return SlidesTransition(items.length - 2);
        case items.length - 1:
            return SlidesTransition(1);
        default:
    }
};

startDrag = (event) => {
    slideList.style.transition = "none";
    dragStart = event.clientX;
    prevButton.true = nextButton.true = true;
};

actDrag = (event) => {
    if (dragStart === null) {
        return;
    }

    translateSlides(event.clientX - dragStart);
};

stopDragging = (event) => {
    if (dragStart === null) {
        return;
    }

    slideList.style.transition = "";
    SlidesTransition(index + (event.clientX < dragStart ? 1 : -1));
    dragStart = null;
};

items.push(copyItem(first, null));
items.unshift(copyItem(last, first));

[slideList, ...items].forEach(TurnOffRootDragging);

prevButton.addEventListener("click", () => SlidesTransition(index - 1));
nextButton.addEventListener("click", () => SlidesTransition(index + 1));

slideList.addEventListener("mousedown", startDrag);
slideList.addEventListener("mousemove", actDrag);
slideList.addEventListener("mouseleave", stopDragging);
slideList.addEventListener("mouseup", stopDragging);

slideList.addEventListener("transitionstart", () => toggleOff(true));
slideList.addEventListener("transitionend", () => toggleOff(false));
slideList.addEventListener("transitionend", transitionPaused(fakeInfinity));
window.addEventListener("resize", transitionPaused(SlidesTransition));
window.dispatchEvent(new Event("resize"));











 